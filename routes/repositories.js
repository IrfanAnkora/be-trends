var express = require('express');
var router = express.Router();
const { Octokit } = require("@octokit/core");
var moment = require('moment');
const fs = require('fs');

async function getRepositories(q, sort, order, per_page, page ) {
  if(!q) {
    throw new Error('Parameter q is required!');
  }
  // To work with octokit, refer to https://github.com/octokit/core.js#readme
  // Create a personal access token at https://github.com/settings/tokens/new?scopes=repo
  const octokit = new Octokit({ auth: process.env.SECRET_KEY });

  // To generate API call, refer to: https://docs.github.com/en/rest/search/search?apiVersion=2022-11-28#search-repositories
  // To construct a search query. refer to: https://docs.github.com/en/rest/search/search?apiVersion=2022-11-28#constructing-a-search-query
  const response = await octokit.request(`GET /search/repositories?q=${encodeURIComponent(q)}&sort=${sort}&order=${order}&per_page=${per_page}&page=${page}`, {
    headers: {
      'accept': 'application/vnd.github+json',
      'X-GitHub-Api-Version': '2022-11-28'
    }
  });
  return response.data;
}


/* GET repositories listing. */
router.get('/', async function(req, res, next) {
  const { q, sort, order, per_page, page } = req.query;

  // Handle required parameter
  if(!q) {
    res.send({message: 'Please specify query parameter q as it is requred'})
  }

  const response = await getRepositories(q, sort, order, per_page, page );

  res.send(response);
});

/* GET repositories listing - last 7 days + highest stars number */
router.get('/seven-days', async function(req, res, next) {
  const { q, sort, order, per_page, page } = req.query;

  // Handle required parameter
  if(!q) {
    res.send({message: 'Please specify query parameter q as it is requred'})
  }

  const response = await getRepositories(q, sort, order, per_page, page);

  const items = response.items;

  const filteredByDay = items.filter((item) => {
    return moment(item['created_at']).format('YYYY-MM-DD HH:mm:ss') 
      >= moment().subtract(360, 'days').startOf('day').format('YYYY-MM-DD HH:mm:ss'); // Specify 7 days
  });

  const filteredByStars = filteredByDay.sort((a, b) => {
    return b.stargazers_count - a.stargazers_count;
  });

  res.send(filteredByStars);
});


/* POST favorite repository by ID */
router.post('/favorite/:id', async function(req, res, next) {
  const { q, sort, order, per_page, page } = req.query;
  const { id } = req.params;

  // Handle required parameter
  if(!q) {
    res.send({message: 'Please specify query parameter q as it is requred'})
  }

  const response = await getRepositories(q, sort, order, per_page, page);

  const items = response.items;

  const favorite = items.find((item) => {
    return item.id === Number(id);
  });


  // Function to check if results.json file exists
  function checkFileExists(callback) {
    fs.access('results.json', fs.constants.F_OK, (err) => {
      if (err) {
        callback(false);
      } else {
        callback(true);
      }
    });
  }

  // Function to create a new results.json file
  function createResultsFile() {
    const data = [];
    const jsonData = JSON.stringify(data, null, 2);

    fs.writeFile('results.json', jsonData, (err) => {
      if (err) throw err;
      console.log('results.json created successfully!');
    });
  }

  // Function to add an object to results.json
  function addObjectToObjectArray(object) {
    checkFileExists((fileExists) => {
      if (!fileExists) {
        createResultsFile();
      }

      fs.readFile('results.json', 'utf8', (err, data) => {
        if (err) throw err;

        const dataArray = JSON.parse(data);

        // Check if element already exists in file, if so, return and don't add duplicate
        const found = dataArray.find(element => element.id === Number(id));
        if(found) return;

        // Element does not exist so add it to the file
        dataArray.push(object);

        const jsonData = JSON.stringify(dataArray, null, 2);

        fs.writeFile('results.json', jsonData, (err) => {
          if (err) throw err;
          console.log('Object added to results.json successfully!');
        });
      });
    });
  }

  // Append an object to results.json
  addObjectToObjectArray(favorite);

  res.send(favorite);
});

/* DELETE favorite repository by ID */
router.delete('/favorite/:id', async function(req, res, next) {
  const { id } = req.params;

  // Function to read all data from results.json
  function readDataFromFile(callback) {
    fs.readFile('results.json', 'utf8', (err, data) => {
      if (err) {
        if (err.code === 'ENOENT') {
          // File doesn't exist
          console.log('results.json does not exist.');
          callback([]);
        } else {
          throw err;
        }
      } else {
        // Parse the JSON data
        const jsonData = JSON.parse(data);
        callback(jsonData);
      }
    });
  }

  // Function to write data to results.json
  function writeDataToFile(data, callback) {
    const jsonData = JSON.stringify(data, null, 2);
    fs.writeFile('results.json', jsonData, (err) => {
      if (err) throw err;
      console.log('Data written to results.json successfully!');
      if (callback) {
        callback();
      }
    });
  }

  // Function to remove an object by ID from results.json
  function removeObjectById(id, callback) {
    readDataFromFile((data) => {
      const updatedData = data.filter((obj) => obj.id !== id);
      writeDataToFile(updatedData, callback);
    });
  }

  // Example usage: Remove an object by ID
  removeObjectById(Number(id), () => {
    console.log(`Object with ID ${id} removed successfully.`);
  });

  res.send({message: 'Repository has been removed from favorites.'});
});

/* GET list all favorite repositories */
router.get('/favorites', function(req, res, next) {

  // Function to read all data from results.json
  function readDataFromFile(callback) {
    fs.readFile('results.json', 'utf8', (err, data) => {
      if (err) {
        if (err.code === 'ENOENT') {
          // File doesn't exist
          console.log('results.json does not exist.');
          callback([]);
        } else {
          throw err;
        }
      } else {
        // Parse the JSON data
        const jsonData = JSON.parse(data);
        callback(jsonData);
      }
    });
  }

  // Read all data from results.json
  readDataFromFile((data) => {
    res.send(data);
  });
});

module.exports = router;
